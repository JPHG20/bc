defmodule BullsAndCows do
  def bulls(secret, guess, x, i) do
    if(i < String.length(secret)) do
      if (String.at(secret, i) == String.at(guess, i)) do
        bulls(secret, guess, x+1, i+1)
      else bulls(secret, guess, x, i+1)
      end
    else x
    end
  end

  def cows(secret, guess, y, i, j) do
    if(i < String.length(secret)) do
      if(j < String.length(guess)) do
        if (String.at(secret, i) == String.at(guess, j)) do
         cows(secret, guess, y+1, i+1, 0)
        else cows(secret, guess, y, i, j+1)  end
      else cows(secret, guess, y, i+1, 0)end
    else y
    end
  end

  def score_guess(secret, guess) do
   if(secret=! guess) do
    b = bulls(secret, guess, 0, 0)
    c = cows(secret, guess, 0, 0, 0)
  if(bulls > 0) do

    "#{b} Bulls, #{c-b} Cows"
  else
    "#{b} Bulls, #{c} Cows"
  end

else if(secret == guess) do
    "You win"
  else

  end


    
  end
end
end

  
